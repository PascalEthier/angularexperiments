import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { NgxUploaderModule } from 'ngx-uploader';
import { AngularFileUploaderModule } from "angular-file-uploader";

import { AppComponent } from './app.component';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    NgxUploaderModule,
    AngularFileUploaderModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
