import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FilterComponent } from './filter/filter/filter.component';
import { ModalViewComponent } from './shared/modal-view/modal-view.component';
import { Page1Component } from './page1/page1.component';
import { Page2Component } from './page2/page2.component';

//This approach might leave some parenthesis in the url, but we can use a UrlSerializer to remove them:
//https://stackoverflow.com/questions/41870911/how-to-remove-parenthesis-in-url-when-using-child-auxiliary-routes-named-route


const routes: Routes = [// Redirecting to keep all of the app under the "/app" prefix. This
// helps deal with some routing issues with empty segments.
{
  path: "",
  pathMatch: "full",
  redirectTo: "app"
},
{
  path:"app",
  children: [
    {
      path: "one",
      component: Page1Component
    },
    {
      path: "two",
      component: Page2Component
    },    
    {
      path: "modal",
      outlet: "modal",
      component: ModalViewComponent,
      children: [
        {
          path: "filter",
          component: FilterComponent
        }
      ]
    }
  ]
}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
