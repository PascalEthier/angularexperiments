import { Component, OnInit } from '@angular/core';
import { ModalViewComponent } from 'src/app/shared/modal-view/modal-view.component';

@Component({
  selector: 'app-filter',
  templateUrl: './filter.component.html',
  styleUrls: ['./filter.component.scss']
})
export class FilterComponent implements OnInit {

  constructor(private modalViewComponent: ModalViewComponent) { }

  ngOnInit() {
  }

  public closeModal() : void {
		this.modalViewComponent.closeModal();
	}
}
